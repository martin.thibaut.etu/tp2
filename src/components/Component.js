export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.tagName == 'img') {
			return `<${this.tagName} ${this.renderAttribute()} />`;
		} else if (this.children == null) {
			return `<${this.tagName} />`;
		} else if (this.children instanceof Array) {
			let tmp = this.renderChildren(this.children);
			return `<${this.tagName} ${this.renderAttribute()}> ${tmp} </${
				this.tagName
			}>`;
		}
		return `<${this.tagName}> ${this.children} </${this.tagName}>`;
	}

	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
	}

	renderChildren(tableau) {
		let chaine = '';
		for (let i = 0; i < tableau.length; i++) {
			if (tableau[i] instanceof Component) {
				chaine += tableau[i].render();
			} else {
				chaine += tableau[i];
			}
		}
		return chaine;
	}
}
